#!/bin/bash

# Setup Default URL with override from positional parameter and environment
PG_URL=${PG_URL-${1:-postgresql://localhost:5432/jupyter}}

# check if pip is installed

P=`pip list | grep pip`
if [ ! "$P" ]; then
  echo "pip is not installed, please install it"
  exit 1
fi


# check if jupyter is installed

PJ=`pip list | grep jupyter`
if [ ! "$PJ" ]; then
  echo "Installing jupyter"
  pip install jupyter
fi


# check if jupyter is installed

PGK=`pip list | grep postgres-kernel`
if [ ! "$PGK" ]; then
  echo "Installing Postgres Kernel"
  pip install git+https://github.com/bgschiller/postgres_kernel.git
fi


DATABASE_URL=$PG_URL jupyter notebook queries.ipynb
